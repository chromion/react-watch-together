const WebSocket = require('ws')

const wss = new WebSocket.Server({ port: 8080 })

const wsState = {
    connections: 0,
    rooms: {}
}

wss.on('connection', (ws, req) => {
    console.log('client connected via %s', req.url)
    wsState.connections += 1
    wsState.rooms[req.url] = wsState.rooms[req.url] + 1 || 1
    ws.url = req.url

    ws.on('close', () => {
        wsState.connections -= 1
        wsState.rooms[req.url] -= 1

        if(wsState.rooms[req.url] === 0) {
            delete wsState.rooms[req.url]
        }
    })

    ws.on('message', msg => {
        wss.clients.forEach(client => {
            if(client.url === req.url) {
                client.send(msg)
            }
        })
    })
})
