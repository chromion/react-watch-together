import React from 'react'
import { useParams } from 'react-router-dom'
import YouTube from 'react-youtube'
import './Room.css'

const getParams = url => {
  const params = {}
  const parser = document.createElement('a')
  parser.href = url
  const query = parser.search.substring(1)
  const vars = query.split('&')

  for(let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=')
    params[pair[0]] = decodeURIComponent(pair[1])
  }

  return params
}

function Room() {
  const { id } = useParams()
  const ws = React.useRef(null)
  const playerRef = React.useRef(null)
  const sliderRef = React.useRef(null)
  const progressRef = React.useRef(null)
  const [videoId, setVideoId] = React.useState(null)
  const [currentTime, setCurrentTime] = React.useState(0)
  const [duration, setDuration] = React.useState(0)

  React.useEffect(() => {
    if(!ws.current) {
      ws.current = new WebSocket(`ws://localhost:8080/${id}`)
      ws.current.onopen = () => {
        console.log('client connected')
      }

      ws.current.onmessage = msg => {
        const message = JSON.parse(msg.data)
        if(message.action === 'set_url') {
          setVideoId(message.videoId)
        }

        const { current: player } = playerRef
        if(message.action === 'play') {
          const { current: slider } = sliderRef
          player.seekTo(0)
          player.playVideo()
        }

        if(message.action === 'pause') {
          player.pauseVideo()
        }
      }
    }
  }, [])

  const onPlayerReady = event => {
    console.log(event)
  }

  const onPlayerStateChange = event => {
    console.log(event)
  }

  const handleSubmit = event => {
    event.preventDefault()
    const url = event.target.url.value
    const regex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/
    const videoId = url.match(regex)[1]
    ws.current.send(JSON.stringify({ videoId, action: 'set_url' }))
  }

  const play = () => {
    ws.current.send(JSON.stringify({ action: 'play' }))
  }

  const pause = () => {
    ws.current.send(JSON.stringify({ action: 'pause' }))
  }

  const onPlay = () => {
    const { current: player } = playerRef
    setDuration(Math.floor(player.getDuration()))
    progressRef.current = setInterval(() => {
      setCurrentTime(Math.floor(player.getCurrentTime()))
    }, 100)
  }

  const onPause = () => {
    clearInterval(progressRef.current)
  }

  const convertTime = timestamp => {
    const minutes = Math.floor(timestamp / 60)
    let seconds = timestamp - (minutes * 60)
    if(seconds < 10) {
      seconds = '0' + seconds
    }
    return minutes + ':' + seconds
  }

  const seekTo = event => {
    setCurrentTime(event.target.value)
    const { current: player } = playerRef
    player.seekTo(event.target.value)
  }

  return (
    <div className="room">
      <h1>Room {id}</h1>
      <form className="url_form" onSubmit={handleSubmit}>
        <input className="url_input" type="text" name="url" placeholder="Url" />
        <button className="load_button" type="submit">Load</button>
      </form>
      <YouTube
        videoId={videoId}
        onReady={e => playerRef.current = e.target}
        onPlay={onPlay}
        onPause={onPause}
        opts={{
          playerVars: {
            controls: 0,
          }
        }}
      />
      <div className="controls">
        <span className="control_button" onClick={play}>
          <svg viewBox="0 0 32 32">
            <path d="M10 2 L10 30 24 16 Z" stroke="#FFFFFF"></path>
          </svg>
        </span>
        <span className="control_button" onClick={pause}>
          <svg viewBox="0 0 32 32">
            <path d="M23 2 L23 30 M9 2 L9 30" stroke="#FFFFFF"></path>
          </svg>
        </span>
        <input ref={sliderRef} className="progress_input" type="range" value={currentTime} max={duration} onChange={seekTo} />
        <span className="progress">
          {convertTime(currentTime)}/{convertTime(duration)}
        </span>
      </div>
    </div>
  )
}

export default Room
