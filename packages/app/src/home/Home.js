import React from 'react';
import { useHistory } from 'react-router-dom'
import './Home.css'

function Home() {
  const history = useHistory()

  const createRoom = () => {
    // Generate random url
    history.push(`/${ Math.random().toString(36).substr(2, 9) }`)
  }

  return (
    <div className="home">
        <h1>React Watch Together</h1>
        <button className="create_room_button" onClick={createRoom}>Create Room</button>
    </div>
  );
}

export default Home;
